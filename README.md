Project:  Project Management System

Name : Rafay Ameen 
Reg : BCS02123008

* How to run tests

1. UnComment the Commented code and Run following file: 

StudentTesting.java located in pms.student package

2.A file will be created and Students will be Added
3.Comment the Uncommented Code again 


4. Do the same for following files:

ProjectTesting.java located in pms.project package
AssignmentTesting.java located in pms.projectassignment package

5. Now you can test the functions of each class accordingly